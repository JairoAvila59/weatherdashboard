package com.example.weatherdashboard.data.network.response

import com.example.weatherdashboard.data.db.entity.CurrentWeatherEntry
import com.example.weatherdashboard.data.db.entity.Location
import com.google.gson.annotations.SerializedName


data class CurrentWeatherResponse(
    @SerializedName("current")
    val currentWeatherEntry: CurrentWeatherEntry,
    val location: Location
)