package com.example.weatherdashboard.data.network

import androidx.lifecycle.LiveData
import com.example.weatherdashboard.data.network.response.CurrentWeatherResponse

interface WeatherNetworkDataSource {

    val downloadedCurrentWeather: LiveData<CurrentWeatherResponse>

    suspend fun fetchCurrentWeather(
        location: String
    )
}