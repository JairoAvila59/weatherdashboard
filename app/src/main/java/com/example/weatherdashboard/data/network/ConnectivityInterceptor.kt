package com.example.weatherdashboard.data.network

import okhttp3.Interceptor

interface ConnectivityInterceptor: Interceptor {
}